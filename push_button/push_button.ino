const int inputPin=13;
const int outputPin=12;

int buttonState;
int lastButtonState=LOW;
int ledState=LOW;

long lastDebounceCount;
long debounceInterval=50;

void setup() {
  // put your setup code here, to run once:
  pinMode(inputPin, INPUT);
  pinMode(outputPin, OUTPUT);

  digitalWrite(outputPin, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  int reading = digitalRead(inputPin);

  if(reading != lastButtonState){
      lastDebounceCount = millis();
    }

  if((millis() - lastDebounceCount) > debounceInterval){
      if(reading != buttonState){
        buttonState = reading; 

        if(buttonState == HIGH){
          ledState = !ledState;
          }
      }
    }

  digitalWrite(outputPin, ledState);
  lastButtonState = reading;
  
}
